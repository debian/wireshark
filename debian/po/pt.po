# Portuguese wireshark debconf translation
# Copyright (C) 2009, the wireshark authors
# This file is distributed under the same license as the wireshark package.
# 2010,2017 Pedro Ribeiro <p.m42.ribeiro@gmail.com>.
#
msgid ""
msgstr ""
"Project-Id-Version: wireshark 2.4.1-1\n"
"Report-Msgid-Bugs-To: wireshark@packages.debian.org\n"
"POT-Creation-Date: 2017-08-07 00:07-0400\n"
"PO-Revision-Date: 2017-09-06 21:26+0100\n"
"Last-Translator: Pedro Ribeiro <p.m42.ribeiro@gmail.com>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Should non-superusers be able to capture packets?"
msgstr "Devem os utilizadores regulares conseguir capturar pacotes?"

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"Dumpcap can be installed in a way that allows members of the \"wireshark\" "
"system group to capture packets. This is recommended over the alternative of "
"running Wireshark/Tshark directly as root, because less of the code will run "
"with elevated privileges."
msgstr ""
"O dumpcap pode ser instalado de maneira que os membros do grupo de sistema "
"\"wireshark\" possam capturar pacotes. Este é o modo recomendado em vez da "
"alternativa de correr o Wireshark/Tshark directamente como root, porque "
"menos código será executado com privilégios elevados."

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"For more detailed information please see /usr/share/doc/wireshark-common/"
"README.Debian."
msgstr ""
"Para informação mais detalhada, leia por favor /usr/share/doc/wireshark-"
"common/README.Debian."

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"Enabling this feature may be a security risk, so it is disabled by default. "
"If in doubt, it is suggested to leave it disabled."
msgstr ""
"Activar esta característica pode ser um risco de segurança, por isso vem "
"desactivada por predefinição. Em caso de dúvida, sugere-se deixá-la "
"desactivada."

#. Type: error
#. Description
#: ../templates:3001
msgid "Creating the wireshark system group failed"
msgstr "A criação do grupo de sistema \"wireshark\" falhou"

#. Type: error
#. Description
#: ../templates:3001
msgid ""
"The wireshark group does not exist, and creating it failed, so Wireshark "
"cannot be configured to capture traffic as an unprivileged user."
msgstr ""
"O grupo de sistema \"wireshark\" não existe e a sua criação falhou, portanto "
"o Wireshark não pode ser configurado para capturar tráfego como utilizador "
"não privilegiado."

#. Type: error
#. Description
#: ../templates:3001
msgid ""
"Please create the wireshark system group and try configuring wireshark-"
"common again."
msgstr ""
"Por favor, crie o grupo de sistema \"wireshark\" e tente configurar "
"novamente o wireshark-common."

#. Type: error
#. Description
#: ../templates:4001
msgid "The wireshark group is a system group"
msgstr "O grupo \"wireshark\" é um grupo de sistema"

#. Type: error
#. Description
#: ../templates:4001
msgid ""
"The wireshark group exists as a user group, but the preferred configuration "
"is for it to be created as a system group."
msgstr ""
"O grupo \"wireshark\" existe como grupo de utilizadores, mas a configuração "
"preferida é tê-lo como grupo de sistema."

#. Type: error
#. Description
#: ../templates:4001
msgid ""
"As a result, purging wireshark-common will not remove the wireshark group, "
"but everything else should work properly."
msgstr ""
"O resultado é que a purga do wireshark-common não irá remover o grupo "
"\"wireshark\", mas tudo o resto deverá funcionar correctamente."

#. Type: error
#. Description
#: ../templates:5001
msgid "Setting capabilities for dumpcap failed"
msgstr "A definição de capacidades para o dumpcap falhou"

#. Type: error
#. Description
#: ../templates:5001
msgid ""
"The attempt to use Linux capabilities to grant packet-capturing privileges "
"to the dumpcap binary failed. Instead, it has had the set-user-id bit set."
msgstr ""
"A tentativa de usar as capacidades Linux para atribuir privilégios de "
"captura de pacotes ao dumpcap falhou. Em vez disso, definiu-se o bit \"set-"
"user-id\"."

#. Type: error
#. Description
#: ../templates:6001
msgid "Removal of the wireshark group failed"
msgstr "A remoção do grupo \"wireshark\" falhou"

#. Type: error
#. Description
#: ../templates:6001
msgid ""
"When the wireshark-common package is configured to allow non-superusers to "
"capture packets the postinst script of wireshark-common creates the "
"wireshark group as a system group."
msgstr ""
"Quando o pacote wireshark-common está configurado para permitir a captura de "
"pacotes por utilizadores não privilegiados o script postinst do wireshark-"
"common cria o grupo \"wireshark\" como grupo do sistema."

#. Type: error
#. Description
#: ../templates:6001
msgid ""
"However, on this system the wireshark group is a user group instead of being "
"a system group, so purging wireshark-common did not remove it."
msgstr ""
"No entanto, neste sistema, o grupo \"wireshark\" é um grupo de utilizador e "
"não do sistema, portanto a purga do wireshark-common não o removeu."

#. Type: error
#. Description
#: ../templates:6001
msgid "If the group is no longer needed, please remove it manually."
msgstr "Se o grupo já não é necessário, remova-o manualmente, por favor."

#~ msgid "Should dumpcap be installed \"setuid root\"?"
#~ msgstr "O dumpcap deve ser instalado com \"setuid root\"?"
